# invitations for various events

## setup

make sure you have lualatex installed

## compile

```
lualatex [tex_file]
```

### auto-compile

```
ls invite-samir-bday3.tex | entr -cn lualatex /_
```
